const { ipcRenderer } = require('electron');

let carsList;

function generateRowLine(tableId, tableData){

    const tbody= document.querySelector(`#${tableId}`);

    tableData.forEach(row => {

        const tr = document.createElement('tr');

        const thId = document.createElement('th');
        thId.scope = 'row';
        thId.innerText = row.id;

        const tdMarque = document.createElement('td');
        tdMarque.innerText = row.brand;

        const tdModele = document.createElement('td');
        tdModele.innerText = row.model;

        const tdActions = document.createElement('td');

        const editBtn = document.createElement('button');
        editBtn.classList.add('btn', 'btn-outline-warning', 'mx-2');
        editBtn.innerText = 'Modif';

        /***************************
         *      UPDATE CAR
         **************************/
        const onClickEditBtn = () => {
             ipcRenderer.send('open-edit-car-window', row);

             ipcRenderer.once('car-edited', (e, editedCar) =>{
                tdMarque.innerText = editedCar.brand;
                tdModele.innerText = editedCar.model;
             });
        };

        editBtn.addEventListener('click', onClickEditBtn);

        const deleteBtn = document.createElement('button');
        deleteBtn.classList.add('btn', 'btn-outline-danger', 'mx-2');
        deleteBtn.innerText = 'Suppr.';

        /**************************
         *      DELETE CAR
         *************************/
        const onClickDeleteBtn = () => {
            ipcRenderer.invoke('show-confirm-delete-car', row.id)
                .then(res => {
                    if(res.choice){
                        tr.remove();
                    }
                });
        };

        deleteBtn.addEventListener('click', onClickDeleteBtn);

        tdActions.append(editBtn, deleteBtn);

        tr.append(thId, tdMarque, tdModele, tdActions);

        tbody.appendChild(tr);
    })
}


/***********************************
 *          INIT DATA
 **********************************/
ipcRenderer.once('init-data', (e, data) => {
    console.log(data);
    carsList = data.cars;
    generateRowLine('cars-table', carsList);
});


/**********************************
 *         ADD  CAR
 *********************************/

function onClickAddCarBtn(e) {
    ipcRenderer.send('open-new-offer-window');
}

const addCarBtn = document.querySelector('#add-car');

addCarBtn.addEventListener('click', onClickAddCarBtn);

/***********************************
 *          NEW CAR ADDED
 **********************************/
ipcRenderer.on('new-car-added', (e, newcar) =>{
   generateRowLine('cars-table', newcar);
});