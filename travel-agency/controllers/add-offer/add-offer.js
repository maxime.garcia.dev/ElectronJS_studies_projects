const { ipcRenderer } = require('electron');

const addOfferForm = document.querySelector('#add-offer-form');
const addOfferSubmit = addOfferForm.querySelector('#add-submit');

const inputTitle = addOfferForm.querySelector('#inputTitle');
const inputDestination = addOfferForm.querySelector('#inputDestination');
const inputShortDesc = addOfferForm.querySelector('#inputShortDescription');
const inputLongDesc = addOfferForm.querySelector('#inputLongDescription');
const inputImageUrl = addOfferForm.querySelector('#inputImageUrl');
const inputPrice = addOfferForm.querySelector('#inputPrice');
const divPlus = addOfferForm.querySelector('#plus');
const addBtn = addOfferForm.querySelector('#btn-add');
const radioCrushTrue = document.querySelector("#crushTrue");
const radioCrushFalse = document.querySelector("#crushFalse");
let inputPlus = divPlus.querySelectorAll('input');
let crushValue;


function onSubmitAddOffer(e) {
    e.preventDefault();

    const plusList = [];

    inputPlus.forEach(plus => {
        plusList.push(plus.value);
    });

    const newOffer = {
        id: null,
        title: inputTitle.value,
        destination: inputDestination.value,
        photo: inputImageUrl.value,
        shortDesc: inputShortDesc.value,
        longDesc: inputLongDesc.value,
        good: plusList,
        crush: crushValue,
        price: inputPrice.value
    }

    ipcRenderer.invoke('new-offer', newOffer)
        .then(msg => {
            const msgDiv = document.querySelector('#response-message-detail');
            msgDiv.textContent = msg;
            msgDiv.hidden = false;

            setTimeout(() => {
                msgDiv.hidden = true;
            }, 2500);
        });
}

addOfferForm.addEventListener('submit', onSubmitAddOffer);

function  onInputCheckValue(){
    for (let plus of inputPlus){
        if (plus.value != '' && inputTitle.value !== '' && inputDestination.value !== ''
            && inputShortDesc.value !== '' && inputLongDesc.value !== '' &&
            inputImageUrl.value !== '' && inputPrice.value > 0 && crushValue != null) {
            addOfferSubmit.hidden = false;
        } else {
            addOfferSubmit.hidden = true;
            break;
        }
    }
}

inputTitle.addEventListener('input', onInputCheckValue);
inputDestination.addEventListener('input', onInputCheckValue);
inputShortDesc.addEventListener('input', onInputCheckValue);
inputLongDesc.addEventListener('input', onInputCheckValue);
inputImageUrl.addEventListener('input', onInputCheckValue);
inputPrice.addEventListener('input', onInputCheckValue);
addBtn.addEventListener('click', onClickAddBtn);
radioCrushTrue.addEventListener('click', onClickRadioBtn);
radioCrushFalse.addEventListener('click', onClickRadioBtn);
inputPlus.forEach((plus) => plus.addEventListener('input', onInputCheckValue));

function onClickAddBtn(){
    const newPlus = document.createElement('input');

    newPlus.type = 'text';
    newPlus.classList.add('form-control', 'inputPlus');
    newPlus.placeholder ='Indiquez un point fort';

    divPlus.append(newPlus);
    addOfferSubmit.hidden = true;
    inputPlus = divPlus.querySelectorAll('input');
    inputPlus.forEach((plus) => plus.addEventListener('input', onInputCheckValue));
};

function onClickRadioBtn(e){
    crushValue = e.target.value === 'true';
    console.log(crushValue);
}