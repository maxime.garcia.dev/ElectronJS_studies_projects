const { ipcRenderer } = require('electron');

const editCarForm = document.querySelector('#edit-car-form');
const editCarSubmit = editCarForm.querySelector('#edit-car-submit');

const brandInput = editCarForm.querySelector('#edit-brand');
const modelInput = editCarForm.querySelector('#edit-model');

function onInputCheckValue(){
    if (brandInput.value !== '' && modelInput.value !== '') {
        editCarSubmit.hidden = false;
    } else {
        editCarSubmit.hidden = true;
    }
}

brandInput.addEventListener('input', onInputCheckValue);
modelInput.addEventListener('input', onInputCheckValue);

function onSubmitEditCar(e){
    e.preventDefault();

    const editedCar = {
        id: null,
        brand: brandInput.value,
        model: modelInput.value
    };

    ipcRenderer.invoke('edit-car', editedCar)
        .then(msg =>{
            const msgDiv = document.querySelector('#response-message');
            msgDiv.textContent = msg;
            msgDiv.hidden = false;

            setTimeout (() => {
                msgDiv.hidden = true;
            }, 2000);
        });
}

editCarForm.addEventListener('submit', onSubmitEditCar);

/***************************
 *     EDIT INIT DATA
 ***************************/
ipcRenderer.once('init-data', (e, carToEdit) => {
    brandInput.value = carToEdit.brand;
    modelInput.value = carToEdit.model;
});