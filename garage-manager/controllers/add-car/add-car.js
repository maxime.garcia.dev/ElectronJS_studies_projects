const { ipcRenderer } = require('electron');

const addCarForm = document.querySelector('#add-car-form');
const addCarSubmit = addCarForm.querySelector('#add-car-submit');

const brandInput = addCarForm.querySelector('#add-brand');
const modelInput = addCarForm.querySelector('#add-model');

function onInputCheckValue(){
    if (brandInput.value !== '' && modelInput.value !== '') {
        addCarSubmit.hidden = false;
    } else {
        addCarSubmit.hidden = true;
    }
}

brandInput.addEventListener('input', onInputCheckValue);
modelInput.addEventListener('input', onInputCheckValue);

function onSubmitAddCar(e){
    e.preventDefault();

    const newCar = {
        brand: brandInput.value,
        model: modelInput.value
    }

    ipcRenderer.invoke('new-car', newCar)
        .then(msg => {
            const msgDiv = document.querySelector('#response-message');
            msgDiv.innerText = msg;
            msgDiv.hidden = false;

            setTimeout(() => {
                msgDiv.hidden = true;
            }, 2000);

            e.target.reset();
            addCarSubmit.hidden = true;
        });
}

addCarForm.addEventListener('submit', onSubmitAddCar);