const { app, BrowserWindow, ipcMain, dialog } = require('electron');
const Store = require('electron-store');
const path = require('path');

const store = new Store();

//decommenté pour clear le storage.
//store.clear();

//Récupération des offres si existantes, auquel cas on créé 2 offres génériques.
let offers = store.has('offers') ? store.get('offers') : [];


//Création d'offre générique si le tableau d'offre est vide.
if (offers.length === 0){
    offers.push({
        id: 1,
        title: 'Club Med Marrakech',
        destination: 'Maroc - Marakech',
        photo: 'https://cdn.pixabay.com/photo/2019/09/24/09/58/marrakech-4500910_1280.jpg',
        shortDesc: 'Vols + Transferts + Club + Tout inclus',
        longDesc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula id eros et vulputate. Vestibulum dapibus mauris sit amet elit porta, at luctus elit maximus. Nullam et consectetur lorem, quis ornare arcu. Morbi nec dignissim sem. Sed placerat venenatis metus vitae hendrerit. Mauris pellentesque posuere feugiat. Vivamus porttitor dictum ante, ac ornare purus facilisis et. Suspendisse blandit felis ante, nec sodales lectus sodales id. Aenean egestas magna ornare, pretium augue nec, sagittis enim. Aliquam rhoncus leo eget nisi dignissim lobortis fermentum vitae turpis. Duis aliquet semper cursus. Aliquam rhoncus dictum lorem. Quisque vitae vulputate mi. Curabitur accumsan libero non quam.',
        good: ['l\'animation','Plage privé','Transats réservés'],
        crush: false,
        price: 999.99
    });
    offers.push({
        id: 2,
        title: 'Club Med Hawaï',
        destination: 'Hawaï - Ohahu',
        photo: 'https://cdn.pixabay.com/photo/2016/03/09/10/28/oahu-1246036_1280.jpg',
        shortDesc: 'Vols + Transferts + Club + Tout inclus',
        longDesc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula id eros et vulputate. Vestibulum dapibus mauris sit amet elit porta, at luctus elit maximus. Nullam et consectetur lorem, quis ornare arcu. Morbi nec dignissim sem. Sed placerat venenatis metus vitae hendrerit. Mauris pellentesque posuere feugiat. Vivamus porttitor dictum ante, ac ornare purus facilisis et. Suspendisse blandit felis ante, nec sodales lectus sodales id. Aenean egestas magna ornare, pretium augue nec, sagittis enim. Aliquam rhoncus leo eget nisi dignissim lobortis fermentum vitae turpis. Duis aliquet semper cursus. Aliquam rhoncus dictum lorem. Quisque vitae vulputate mi. Curabitur accumsan libero non quam.',
        good: ['l\'animation','Plage privé','Transats réservés'],
        crush: true,
        price: 1749.99
    });

   store.set('offers', offers);
}

let mainWindow = null;
let detailWin = null;
let addWin = null;

//Fonction générique de création de fenêtre.
function createWindow (viewPath, width=1980, height = 1200) {
    const win = new BrowserWindow({
        width: width,
        height: height,
        webPreferences: {
            nodeIntegration: false,
            contextIsolation: true,
            enableRemoteModule: false,
            preload: path.join(__dirname, 'preload.js')
        }
    });

    win.loadFile(path.join(__dirname, viewPath));

    return win;
}

//Création de la fenêtre home au démarrage de l'app
app.whenReady().then(() => {
    mainWindow = createWindow('views/home/home.html');

    mainWindow.webContents.on('did-finish-load', () => {
        mainWindow.webContents.send('init-data', offers);
    });
});

//Création de la fenêtre d'ajout.
ipcMain.on('open-new-offer-window', (e) => {
    addWin = createWindow('views/add-offer/add-offer.html', 1200, 1000);

    //On récupère l'offre créé afin de l'ajouter au store pour la persistance de données.
    ipcMain.handle('new-offer', (e, newOffer) => {
       if (offers.length > 0) {
           newOffer.id = offers[offers.length - 1].id + 1;
       }
       else {
           newOffer.id = 1;
       }

       offers.push(newOffer);

       store.set('offers', offers);

       //On renvoie la nouvelle offre afin que la page homme soit mise à jour avec la nouvelle offre.
       mainWindow.webContents.send('new-offer-added', [newOffer]);

       return 'Cette offre a été ajoutée';
    });

    addWin.on('closed', () => {
       ipcMain.removeHandler('new-offer');
    });
});

//Création de la fenêtre d'affichage des détails de l'offre
ipcMain.on('open-detail-offer-window', (e, offer) => {
    detailWin = createWindow('views/detail/detail.html', 1200, 1000);

    detailWin.webContents.on('did-finish-load', () => {
        detailWin.webContents.send('init-detail-data', offer);
    });
});

//Création de la fenêtre d'édition de l'offrte
ipcMain.on('open-edit-offer-window', (e, offer) =>{
    const win = createWindow('views/edit-offer/edit-offer.html', 1200, 1000);

    win.webContents.on('did-finish-load', () => {
        win.webContents.send('init-edit-data', offer);
    });

    //On récupère l'offre édité afin de la remplacer dans le store pour la persistance de données.
    ipcMain.handle('editing-offer', (e, editedOffer) => {
        editedOffer.id = offer.id;

        //console.log(editedOffer);
        for (let [index, searchOffer] of offers.entries()){
           if (searchOffer.id === editedOffer.id){
               offers[index] = editedOffer;
               store.set('offers', offers);
               break;
           }
        }

        //On renvoie l'offre édité dans le home et le detail afin de mettre à jour les données.
        mainWindow.webContents.send('edited-offer', editedOffer);

        detailWin.webContents.send('edited-offer', editedOffer);

        return 'Cette offre a bien été modifiée ;)';
    });

    win.on('closed', () => {
        ipcMain.removeHandler('editing-offer');
    });
});

//Ouverture de la pop-up de confirmation de suppression d'une offre.
ipcMain.on('deleting-offer', (e, offerId) =>{
    const choice = dialog.showMessageBoxSync({
        type: 'warning',
        buttons: ['Non', 'Oui'],
        title: 'Confirmation supression offre',
        message: 'Êtes vous sûr de vouloir supprimer cette offre?'
    });

    if(choice){
        for (let [index, offerToDelete] of offers.entries()){
            if (offerToDelete.id === offerId){
                offers.splice(index, 1);
                store.set('offers', offers);
                break;
            }
        }
    }

    //On met à jour le home afin d'aficher la liste d'offres sans celle que l'on vien de supprimer.
    mainWindow.webContents.send('deleted-offer', choice);

    detailWin.close();
});

