const { app, BrowserWindow, ipcMain, dialog } = require('electron');
const Store = require('electron-store');
const path = require('path');

const store = new Store();

const cars = store.has('cars') ? store.get('cars') : [];

if (cars.length === 0){
    cars.push({ id:1 , brand: 'Renault', model: 'Clio' });
    cars.push({ id:2 , brand:'Dodge', model: 'Charger' });
    cars.push({ id:3 , brand:'Nissan', model: 'Skyline' });

    store.set('cars', cars);
};

let mainWindow = null;

function createWindow (viewPath, width = 1400, height = 1000) {
    const win = new BrowserWindow({
        width: width,
        height: height,
        webPreferences: {
            nodeIntegration: false,
            contextIsolation: true,
            enableRemoteModule: false,
            preload: path.join(__dirname, 'preload.js')
        }
    });

    win.loadFile(path.join(__dirname, viewPath));

    return win;

}

app.whenReady().then(() => {
    mainWindow = createWindow('views/home/home.html');

    mainWindow.webContents.on('did-finish-load', () => {
        mainWindow.webContents.send('init-data', { cars });
    });
});

ipcMain.on('open-new-car-window', (e, cars) => {

    const win = createWindow('views/add-car/add-car.html', 1000, 600);

    ipcMain.handle('new-car', (e, newCar) =>{
        newCar.id = 1;

        if(cars.length > 0) {
            newCar.id = cars[cars.length - 1].id + 1;
        }

        cars.push(newCar);

        store.set('cars', cars);

        mainWindow.webContents.send('new-car-added', [newCar]);

        return 'Cette voiture a été ajouté.';
    });

    win.on('closed', () => {
        ipcMain.removeHandler('new-car');
    });
});

ipcMain.handle('show-confirm-delete-car', (e, carId) =>{
    const choice = dialog.showMessageBoxSync({
        type: 'warning',
        buttons: ['Non', 'Oui'],
        title: 'Confirmation supression voiture',
        message: 'Êtes vous sûr de vouloir supprimer cette voiture?'
    });

    if(choice){
        for(let [index, car] of cars.entries()){
            if (car.id === carId){
                cars.splice(index, 1);
                break;
            }
        }

        store.set('cars', cars);
    }
    return { choice, cars };
});

ipcMain.on('open-edit-car-window', (e, carDatas) => {
    const win = createWindow('views/edit-car/edit-car.html', 1000, 600);



    ipcMain.handle('edit-car', (e, editedCarDatas) =>{
        editedCarDatas.id = carDatas.id;

        for (let [index, car] of cars.entries()){
            if(car.id === editedCarDatas.id){
                cars.splice(index, 1 , editedCarDatas);
                break;
            }
        }

        store.set('cars', cars);

        mainWindow.webContents.send('car-edited', editedCarDatas);

        return 'Cette voiture a bien été modifiée ;)';
    });

    win.on('closed', () => {
        ipcMain.removeHandler('edit-car');
    });
});