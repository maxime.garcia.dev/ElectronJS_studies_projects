const { ipcRenderer } = require('electron');

const editOfferForm = document.querySelector('#edit-offer-form');
const editOfferSubmit = editOfferForm.querySelector('#edit-submit');

const inputTitle = editOfferForm.querySelector('#inputTitle');
const inputDestination = editOfferForm.querySelector('#inputDestination');
const inputShortDesc = editOfferForm.querySelector('#inputShortDescription');
const inputLongDesc = editOfferForm.querySelector('#inputLongDescription');
const inputImageUrl = editOfferForm.querySelector('#inputImageUrl');
const inputPrice = editOfferForm.querySelector('#inputPrice');
const divPlus = editOfferForm.querySelector('#plus');
const addBtn = editOfferForm.querySelector('#btn-add');
const radioCrushTrue = document.querySelector("#crushTrue");
const radioCrushFalse = document.querySelector("#crushFalse");
let inputPlus;
let crushValue;

function  onInputCheckValue(){
    for (let plus of inputPlus){
        if (plus.value != '' && inputTitle.value !== '' && inputDestination.value !== ''
            && inputShortDesc.value !== '' && inputLongDesc.value !== '' &&
            inputImageUrl.value !== '' && inputPrice.value > 0 && crushValue != null) {
            editOfferSubmit.hidden = false;
        } else {
            editOfferSubmit.hidden = true;
            break;
        }
    }
}

function onSubmitEditOffer(e){
    e.preventDefault();

    console.log(crushValue);
    const plusList = [];

    inputPlus.forEach(plus => {
       plusList.push(plus.value);
    });

    const editedOffer = {
        id: null,
        title: inputTitle.value,
        destination: inputDestination.value,
        photo: inputImageUrl.value,
        shortDesc: inputShortDesc.value,
        longDesc: inputLongDesc.value,
        good: plusList,
        crush: crushValue,
        price: inputPrice.value
    }

    ipcRenderer.invoke('editing-offer', editedOffer)
        .then(msg => {
           const msgDiv = document.querySelector('#response-message-detail');
           msgDiv.textContent = msg;
           msgDiv.hidden = false;

           setTimeout(() => {
               msgDiv.hidden = true;
           }, 2500);
        });
}

editOfferForm.addEventListener('submit', onSubmitEditOffer);

function onClickAddBtn(){
    const newPlus = document.createElement('input');

    newPlus.type = 'text';
    newPlus.classList.add('form-control', 'inputPlus');
    newPlus.placeholder ='Indiquez un point fort';

    divPlus.append(newPlus);
    editOfferSubmit.hidden = true;
    inputPlus = divPlus.querySelectorAll('input');
    setEventListener();
};

function onClickRadioBtn(e){
    crushValue = e.target.value === 'true';
}



/***********************************
 *       SET LISTENERS
 **********************************/

function setEventListener(){
    inputTitle.addEventListener('input', onInputCheckValue);
    inputDestination.addEventListener('input', onInputCheckValue);
    inputShortDesc.addEventListener('input', onInputCheckValue);
    inputLongDesc.addEventListener('input', onInputCheckValue);
    inputImageUrl.addEventListener('input', onInputCheckValue);
    inputPrice.addEventListener('input', onInputCheckValue);
    inputPlus.forEach((plus) => plus.addEventListener('input', onInputCheckValue));
    addBtn.addEventListener('click', onClickAddBtn);
    radioCrushTrue.addEventListener('click', onClickRadioBtn);
    radioCrushFalse.addEventListener('click', onClickRadioBtn);
}


/****************************
 *       INIT DATA
 ****************************/
ipcRenderer.once('init-edit-data', (e, offer) => {
    inputTitle.value = offer.title;
    inputDestination.value = offer.destination;
    inputShortDesc.value = offer.shortDesc;
    inputLongDesc.value = offer.longDesc;
    inputImageUrl.value = offer.photo;
    inputPrice.value = offer.price;
    crushValue = offer.crush;

    console.log(crushValue);
    if (crushValue){
        document.getElementById("crushTrue").checked = true;
    } else {
        document.getElementById("crushFalse").checked = true;
    }

    offer.good.forEach(plus => {
       const newInputPlus = document.createElement('input');
        newInputPlus.type ='text';
        newInputPlus.classList.add('form-control', 'inputPlus');
        newInputPlus.value = plus;

        divPlus.append(newInputPlus);
    });

    inputPlus = divPlus.querySelectorAll('input');
    setEventListener();
});