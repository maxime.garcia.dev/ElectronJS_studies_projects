const { ipcRenderer } = require('electron');

function generateCards(listId, offers){

    const offersList = document.querySelector(`#${listId}`);

    offers.forEach(offer => {

        const card = document.createElement('div');
        card.classList.add('card', 'shadow', 'mx-auto', 'col-8', 'mb-5');
        card.style.backgroundColor = '#545454';

        const onClickCard = () => {
            ipcRenderer.send('open-detail-offer-window', offer);

            /************************
             *  EDIT OFFER CARD
             ***********************/
            ipcRenderer.once('edited-offer', (e, editedOffer) => {
                offer = editedOffer;
                title.innerHTML = `<h3>${editedOffer.title}</h3>`;
                destination.innerHTML = `<span class="font-weight-bold">${editedOffer.destination}</span>`;
                photo.innerHTML = `<img src="${editedOffer.photo}" width="550px" height="300px">`;
                shortDesc.innerHTML = `<span>${editedOffer.shortDesc}</span>`;
                good.innerHTML = `<span class="font-weight-bold">Points Forts:</span>`
                const list = document.createElement('ul');
                editedOffer.good.forEach(point => {
                    const plus = document.createElement('li');
                    plus.innerText = point;

                    list.append(plus);
                });
                good.append(list);
                price.innerHTML =
                    `<span class="small">dès</span><span class="h1">${editedOffer.price}</span><span class="align-top">€</span>`;
                crush.hidden = editedOffer.crush !== true;
            });
            /************************
             *  DELETE OFFER CARD
             ************************/
            ipcRenderer.once('deleted-offer', (e, choice)=> {
               if (choice){
                   card.remove();
               }
            });
        }

        card.addEventListener('click', onClickCard);

        const head = document.createElement('div');
        head.classList.add('d-inline-flex', 'justify-content-between', 'mt-4');
        head.style.color = '#FFC300';

        const title = document.createElement('div')
        title.classList.add('mx-2');
        title.innerHTML = `<h3>${offer.title}  <i class="fa fa-plane"></i></h3>`;

        const destination = document.createElement('div');
        destination.classList.add('mx-2', 'pt-2');
        destination.innerHTML = `<span class="font-weight-bold">${offer.destination}</span>`

        head.append(title, destination);

        const body = document.createElement('div');
        body.classList.add('d-inline-flex');

        const photo = document.createElement('div')
        photo.classList.add('ml-2', 'my-3');
        photo.innerHTML = `<img src="${offer.photo}" width="550px" height="300px">`;

        const descAndGood = document.createElement('div');
        descAndGood.classList.add('mr-auto', 'ml-4');
        descAndGood.style.color = '#ffc300';
        descAndGood.innerHTML = `<hr>`;

        const shortDesc = document.createElement('div');
        shortDesc.classList.add('mb-5');
        shortDesc.innerHTML = `<span>${offer.shortDesc}</span>`;

        const good = document.createElement('div');
        good.innerHTML = `<span style="text-decoration: underline #FFC300" class="font-weight-bold">Points Forts:</span>`

        const list = document.createElement('ul');

        offer.good.forEach(point => {
            const plus = document.createElement('li');
            plus.innerText = point;

            list.append(plus);
        });

        good.append(list);

        descAndGood.append(shortDesc, good);

        const crushAndPrice = document.createElement('div');
        crushAndPrice.classList.add('mx-4', 'mt-4');
        crushAndPrice.style.color = '#DE893A';

        const crush = document.createElement('div');
        crush.classList.add('mb-2', 'text-center');
        crush.style.fontWeight = 'bold';
        crush.innerHTML = `<i class="fa fa-heart"></i> Coup de coeur! <i class="fa fa-heart"></i>`;
        console.log(offer);
        if (offer.crush === true){
            crush.hidden = false;
        } else {
            crush.hidden = true;
        }

        const price = document.createElement('div');
        price.classList.add('p-2', 'shadow');
        price.style.backgroundColor = '#9C9C9C';
        price.innerHTML =
            `<span class="small">dès</span><span class="h1">${offer.price}</span><span class="align-top">€</span>`;

        crushAndPrice.append(crush, price);

        body.append(photo, descAndGood, crushAndPrice);

        card.append(head, body);

        offersList.appendChild(card);
    });
}

/*******************************
 *         ADD OFFER
 ******************************/

function onClickAddBtn() {
    ipcRenderer.send('open-new-offer-window');
}

const addBtn = document.querySelector('#add-offer');

addBtn.addEventListener('click', onClickAddBtn);

/*******************************
 *        NEW OFFER ADDED
 ******************************/

ipcRenderer.on('new-offer-added', (e, newOffer) => {
    generateCards('offers-list', newOffer);
});

/*********************************
 *          INIT DATA
 ********************************/
ipcRenderer.once('init-data', (e, data) => {
   generateCards('offers-list', data);
});