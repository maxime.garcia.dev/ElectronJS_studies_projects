const { ipcRenderer } = require('electron');

function fillDetailPage(pageId, offer){

    const plus = document.querySelector('#plus');
    const photo = document.querySelector('#photo');
    const title = document.querySelector('#title');
    const destination = document.querySelector('#destination');
    const longDesc = document.querySelector('#description');
    const crush = document.querySelector('#crush');
    const price = document.querySelector('#amount');
    photo.style.backgroundImage = `url("${offer.photo}")`;
    photo.style.backgroundPosition = 'center';

    title.textContent = offer.title;
    destination.textContent = offer.destination;
    longDesc.innerHTML += `<p style="word-wrap: break-word">${offer.longDesc}</p>`;

    const good = document.createElement('ul')
    good.style.listStyleType = 'none';
    good.style.padding = '0';
    good.style.margin = '0';
    offer.good.forEach(point => {
       const li = document.createElement('li');
       li.innerText = `- ${point}`;
       good.append(li);
    });

    plus.append(good);

    console.log(offer.crush);
    if (offer.crush == true){
        crush.hidden = false;
    } else {
        crush.hidden = true;
    }
    price.textContent = offer.price

    /**************************
     *      EDIT GESTION
     **************************/
    const onClickEditBtn = () => {
        ipcRenderer.send('open-edit-offer-window', offer);

        ipcRenderer.once('edited-offer', (e, editedOffer) =>{
            offer = editedOffer;
            photo.style.backgroundImage = `url("${editedOffer.photo}")`;
            title.textContent = editedOffer.title;
            destination.textContent = editedOffer.destination;
            longDesc.innerHTML = `<span style="font-weight: bold; text-decoration: underline black">Description de l'offre:</span><br>`;
            longDesc.innerHTML += `<p style="word-wrap: break-word">${editedOffer.longDesc}</p>`;
            good.innerHTML = '';

            editedOffer.good.forEach(point => {
                const li = document.createElement('li');
                li.innerText = `- ${point}`;
                good.append(li);
            });

            plus.append(good);
            if (editedOffer.crush === true){
                crush.hidden = false;
            } else {
                crush.hidden = true;
            }
            price.textContent = editedOffer.price;
        });
    };

    const editBtn = document.querySelector('#edit-offer');
    editBtn.addEventListener('click', onClickEditBtn);

    /***************************
     *     DELETE GESTION
     **************************/

    const onClickDeleteBtn = () => {
        ipcRenderer.send('deleting-offer', offer.id);
    }

    const deleteBtn = document.querySelector('#delete-offer');
    deleteBtn.addEventListener('click', onClickDeleteBtn);
}

ipcRenderer.once('init-detail-data', (e, offer) => {
   fillDetailPage('detail-page', offer);
});